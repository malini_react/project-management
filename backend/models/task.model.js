const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const taskSchema = new Schema(
  {
    taskname: {
      type: String,
      required: true,
    },
    taskDesc: {
      type: String,
      required: true,
    },
    projectname: {
      type: String,
      required: true,
    },
    assignTo: {
      type: String,
      required: true,
    },
    userPay: {
      type: Number,
      required: true,
    },
    taskPay: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    startDate: {
      type: String,
      required: true,
    },
    endDate: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;
