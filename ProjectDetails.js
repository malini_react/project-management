import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';

const ProjectDetails = ({route}) => {
  const [response, setResponse] = useState([]);
  var screenView = useIsFocused();

  const [users, setUsers] = useState([]);
  const [focus, setFocus] = useState(screenView);
  const [projectStatus, setProjectStatus] = useState([]);

  const {item} = route.params;
  const {verifiedUserName} = route.params;

  var projectName = item.projectname;
  const {valueTwo} = route.params;

  const navigation = useNavigation();

  useEffect(() => {
    callUsersApi();
    callTaskGet();
    setFocus(screenView);
  }, [focus, screenView]);

  function callUsersApi() {
    fetch('http://192.168.0.188:5000/users', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setUsers(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callTaskGet() {
    fetch('http://192.168.0.188:5000/task', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
          checkProjectStatus(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function checkProjectStatus(res) {
    console.log('array1', res);

    var newArr = [];

    res.map(x => {
      if (x.projectname == projectName) {
        newArr.push(x);
        setProjectStatus(...newArr);
        console.log('array', newArr);
      }
    });
    if (newArr.length != 0) {
      callCheck(newArr);
    }
  }

  function callCheck(newArr) {
    console.log('check called');

    var comArr = [];
    var incomArr = [];
    newArr.map(y => {
      if (y.status == 'Completed') {
        comArr.push(y);
      } else {
        incomArr.push(y);
      }
    });

    callApi(incomArr);
  }

  function callApi(incomArr) {
    console.log('check called');
    if (incomArr.length == 0) {
      console.log('no pending works');
      callUpdateApi();
    } else {
      console.log('pending works', item);
    }
  }

  function callUpdateApi() {
    var id = item._id;
    fetch('http://192.168.0.188:5000/project/update/' + id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        projectname: item.projectname,
        username: item.username,
        status: 'Completed',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project updated!') {
          console.log('updated');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={{
          margin: 10,
          width: '95%',
          height: 40,
          backgroundColor: 'green',
          justifyContent: 'center',
        }}
        onPress={() => navigation.navigate('NewTask', {item})}>
        <Text
          style={[
            styles.addText,
            {color: 'white', fontWeight: '400', fontSize: 14},
          ]}>
          Add new task
        </Text>
      </TouchableOpacity>
      <View>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={items => items.id}
            renderItem={({item}) => (
              <View>
                {item.projectname == projectName &&
                (item.assignTo == verifiedUserName ||
                  verifiedUserName == 'malini@gmail.com') ? (
                  <TouchableOpacity
                    style={styles.touchableOpacityStyle}
                    onPress={() =>
                      navigation.navigate('Task', {item, verifiedUserName})
                    }>
                    <View style={{marginTop: 10, marginBottom: 10}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={styles.textStyle}>{item.taskname}</Text>
                        <Text
                          style={
                            item.status == 'Completed'
                              ? styles.textStyle1
                              : styles.textStyle
                          }>
                          {item.status}
                        </Text>
                      </View>
                      <Text style={(styles.textStyle, {color: 'grey'})}>
                        {item.taskDesc}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ) : null}
              </View>
            )}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3D3D3',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 5,
    margin: 10,
    borderRadius: 5,
  },
  textStyle: {
    margin: 10,
    color: 'black',
  },
  textStyle1: {
    margin: 10,
    color: 'green',
    fontWeight: '400',
  },
});

export default ProjectDetails;
