import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation, useIsFocused} from '@react-navigation/native';

const Project = ({route}) => {
  const {verifiedUserJob} = route.params;
  const {verifiedUserName} = route.params;

  var screenView = useIsFocused();

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const [focus, setFocus] = useState(screenView);

  const navigation = useNavigation();

  useEffect(() => {
    callProjectGet();
    setFocus(screenView);
  }, [screenView, focus]);

  function callProjectGet() {
    fetch('http://192.168.0.188:5000/project', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setResponse(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      {verifiedUserJob == 'admin' ? (
        <View>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() =>
              navigation.navigate('NewProject', {
                verifiedUserName,
                verifiedUserJob,
              })
            }>
            <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
              Add new project
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() =>
              navigation.navigate('Register', {
                verifiedUserName,
                verifiedUserJob,
              })
            }>
            <Text style={[styles.addText, {fontWeight: '400', fontSize: 14}]}>
              Add new member
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
      <View>
        {response != null && response.length > 0 ? (
          <FlatList
            data={response}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <View
                style={{
                  padding: 10,
                  backgroundColor: 'white',
                  margin: 10,
                  borderRadius: 5,
                }}>
                <TouchableOpacity
                  style={styles.touchableOpacityStyle}
                  onPress={() => {
                    navigation.navigate('Tasks', {item, verifiedUserName});
                  }}>
                  <View style={{marginTop: 10, marginBottom: 10}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles.textStyle}>
                        Project name : {item.projectname}
                      </Text>
                      <Text
                        style={
                          item.status == 'Completed'
                            ? styles.textStyle1
                            : styles.textStyle
                        }>
                        {item.status}
                      </Text>
                    </View>
                    <Text style={styles.textStyle}>Created by : 'admin'</Text>
                    <Text style={styles.textStyle}>
                      Created on : Nov 30, 2021
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3D3D3',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: 'green',
    justifyContent: 'center',
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
  },
  textStyle1: {
    margin: 10,
    color: 'green',
    fontWeight: '400',
  },
});

export default Project;
