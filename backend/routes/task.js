const router = require('express').Router();
let Task = require('../models/task.model');

router.route('/').get((req, res) => {
  Task.find()
    .then(task => res.json(task))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Task.findById(req.params.id)
    .then(task => res.json(task))
    .catch(err => res.status(400).json('Error: ' + err));
});
// router.route('/:id').delete((req, res) => {
//   Task.findByIdAndDelete(req.params.id)
//     .then(() => res.json('Task deleted.'))
//     .catch(err => res.status(400).json('Error: ' + err));
// });
router.route('/update/:id').post((req, res) => {
  Task.findById(req.params.id)
    .then(task => {
      task.taskname = req.body.taskname;
      task.taskDesc = req.body.taskDesc;
      task.projectname = req.body.projectname;
      task.assignTo = req.body.assignTo;
      task.userPay = req.body.userPay;
      task.taskPay = req.body.taskPay;
      task.status = req.body.status;
      task.startDate = req.body.startDate;
      task.endDate = req.body.endDate;
      task
        .save()
        .then(() => res.json('Task updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const taskname = req.body.taskname;
  const taskDesc = req.body.taskDesc;
  const projectname = req.body.projectname;
  const assignTo = req.body.assignTo;
  const userPay = req.body.userPay;
  const taskPay = req.body.taskPay;
  const status = req.body.status;
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const newTask = new Task({
    taskname,
    taskDesc,
    projectname,
    assignTo,
    userPay,
    taskPay,
    status,
    startDate,
    endDate,
  });

  newTask
    .save()
    .then(() => res.json('Task added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;
