import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const NewProject = ({route}) => {
  const {verifiedUserJob} = route.params;
  const {verifiedUserName} = route.params;

  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueTwo, setValueTwo] = useState('');
  const [dummy, setDummy] = useState('dummy');

  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);

  const navigation = useNavigation();

  function saveProjectData() {
    if (valueOne != '' && valueTwo != '') {
      setData([
        ...data,
        {id: valueOne, name: valueTwo, createdBy: verifiedUserName, tasks: []},
      ]);
      setClick(false);
      // setValueOne('');
      // setValueTwo('');
      callProject();
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  function callProject() {
    fetch('http://192.168.0.188:5000/project/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        projectname: valueTwo,
        username: verifiedUserName,
        status: 'Pending',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Project added!') {
          navigation.navigate('Home', {verifiedUserName, verifiedUserJob});
        } else {
          console.log('Add project failed');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{backgroundColor: 'white'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Project Id</Text>
          <TextInput
            value={valueOne}
            style={styles.textInput}
            onChangeText={text => setValueOne(text)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Project name</Text>
          <TextInput
            value={valueTwo}
            style={styles.textInput}
            onChangeText={text => setValueTwo(text)}
          />
        </View>
        <TouchableOpacity
          style={styles.textInputAddBtn}
          onPress={() => saveProjectData()}>
          <Text style={styles.addText}>Save</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D3D3D3',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 40,
    backgroundColor: 'green',
    justifyContent: 'center',
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
  },
  addText: {
    alignSelf: 'center',
    color: 'white',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    justifyContent: 'center',
  },
  textStyle: {
    margin: 2,
  },
});

export default NewProject;
