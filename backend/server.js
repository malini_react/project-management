const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {useNewUrlParser: true});
const connection = mongoose.connection;
connection.once('open', () => {
  console.log('MongoDB database connection established successfully');
});

const usersRouter = require('./routes/users');
const projectRouter = require('./routes/project');
const taskRouter = require('./routes/task');

app.use('/users', usersRouter);
app.use('/project', projectRouter);
app.use('/task', taskRouter);

app.listen(port, () => {
  console.log('server started at port 5000');
});
