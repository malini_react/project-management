import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';

const TaskDetails = ({route}) => {
  const {item} = route.params;

  console.log('task ', item);

  const navigation = useNavigation();

  const [modalVisible, setModalVisible] = useState(false);
  const [valueNine, setValueNine] = useState('');
  const [selectedValue, setSelectedValue] = useState('Pending');
  const [valueFive, setValueFive] = useState('');

  //   useEffect(() => {

  //   }, []);

  function saveData() {
    setValueNine('');
    setModalVisible(false);
    navigation.goBack();
  }

  function updateTask(text) {
    var id = item._id;
    var pay = item.userPay * parseInt(valueFive);
    console.log('pay', pay);
    fetch('http://192.168.0.188:5000/task/update/' + id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        taskname: item.taskname,
        taskDesc: item.taskDesc,
        projectname: item.projectname,
        assignTo: item.assignTo,
        userPay: item.userPay,
        taskPay: pay,
        status: selectedValue,
        startDate: item.startDate,
        endDate: item.endDate,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Task updated!') {
          console.log('updated');
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Project </Text>
          <Text style={{alignSelf: 'center', width: '50%', color: 'black'}}>
            {item.projectname}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task </Text>
          <Text style={{alignSelf: 'center', width: '50%', color: 'black'}}>
            {item.taskname}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task desc </Text>
          <Text style={{alignSelf: 'center', width: '50%', color: 'black'}}>
            {item.taskDesc}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Duration </Text>
          <Text style={{alignSelf: 'center', width: '50%', color: 'black'}}>
            {item.startDate} - {item.endDate}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Team </Text>
          <Text style={{alignSelf: 'center', width: '50%', color: 'black'}}>
            {item.assignTo} ({item.pay}/hr)
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
            padding: 20,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Status </Text>
          <Picker
            selectedValue={selectedValue}
            style={{
              height: 30,
              alignSelf: 'center',
              width: '50%',
              color: 'black',
            }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }>
            <Picker.Item label="Pending" value="Pending" />
            <Picker.Item label="Completed" value="Completed" />
          </Picker>
        </View>
        {selectedValue == 'Completed' ? (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
              padding: 20,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>
              Hours worked{' '}
            </Text>
            <TextInput
              value={valueFive}
              style={{
                alignSelf: 'center',
                width: '50%',
                borderWidth: 1,
                borderColor: 'grey',
              }}
              onChangeText={text => setValueFive(text)}
              onSubmitEditing={text => updateTask(text)}
            />
          </View>
        ) : null}

        {/* {item.completed == undefined ? (
          <TouchableOpacity
            style={styles.addBtn}
            onPress={() => setModalVisible(true)}>
            <Text style={[styles.addText, {color: 'white'}]}>Completed</Text>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginLeft: 10,
              padding: 20,
            }}>
            <Text style={{alignSelf: 'center', width: '35%'}}>Status </Text>
            <Text style={{alignSelf: 'center', width: '45%'}}>Completed</Text>
          </View>
        )} */}
      </View>
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>
                Number of hours worked
              </Text>
              <TextInput
                value={valueNine}
                style={styles.textInput}
                onChangeText={text => setValueNine(text)}
              />
            </View>
            <TouchableOpacity style={styles.addBtn} onPress={() => saveData()}>
              <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default TaskDetails;
