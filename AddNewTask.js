import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
  FlatList,
  Modal,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';

const NewTask = ({route}) => {
  const [click, setClick] = useState(false);
  const [valueOne, setValueOne] = useState('');
  const [valueFour, setValueTwo] = useState('');
  const [valueThree, setValueThree] = useState('');
  const [valueSix, setValueSix] = useState('');
  const [valueFive, setValueFive] = useState('');
  const [data, setData] = useState([]);
  const [response, setResponse] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [valueSeven, setValueSeven] = useState('');
  const [valueEight, setValueEight] = useState('');
  const [team, setTeam] = useState([]);
  const [users, setUsers] = useState([]);
  const [selectedValue, setSelectedValue] = useState('');

  const {item} = route.params;
  const {verifiedUserName} = route.params;

  const navigation = useNavigation();

  useEffect(() => {
    callUsersApi();
  }, []);

  function callUsersApi() {
    fetch('http://192.168.0.188:5000/users', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson != null) {
          setUsers(responseJson);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function showTextInput() {
    setClick(true);
  }

  function saveTaskData() {
    if (
      valueOne != '' &&
      valueFour != '' &&
      valueThree != '' &&
      valueSix != '' &&
      valueFive != ''
    ) {
      setData([
        ...data,
        {
          id: valueOne,
          name: valueFour,
          description: valueThree,
          startDate: valueSix,
          endDate: valueFive,
          teamMembers: team,
        },
      ]);
      callTask();

      setClick(false);
      setValueOne('');
      setValueTwo('');
      setValueThree('');
      setValueSix('');
      setValueFive('');
      // Alert.alert('Tasks created successfully ');
    } else {
      Alert.alert('Please enter all the fields');
    }
  }

  function callTask() {
    fetch('http://192.168.0.188:5000/task/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Connection: 'keep-alive',
      },
      body: JSON.stringify({
        taskname: valueFour,
        taskDesc: valueThree,
        projectname: item.projectname,
        assignTo: selectedValue,
        userPay: valueEight,
        taskPay: 0,
        status: 'Pending',
        startDate: valueSix,
        endDate: valueFive,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson == 'Task added!') {
          callTaskGet();
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  function callTaskGet() {
    fetch('http://192.168.0.188:5000/task', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        setResponse(responseJson);
        navigation.navigate('Tasks', {item, verifiedUserName});
      })
      .catch(error => {
        console.error(error);
      });
  }

  function saveTeamMembers() {
    setModalVisible(false);
  }

  function openModal(itemValue) {
    setSelectedValue(itemValue);
    setModalVisible(true);
  }

  return (
    <SafeAreaView style={styles.container}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>
                Team member name
              </Text>
              <TextInput
                value={selectedValue}
                style={styles.textInput}
                editable={false}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginLeft: 10,
              }}>
              <Text style={{alignSelf: 'center', width: '35%'}}>Pay/hour</Text>
              <TextInput
                value={valueEight}
                style={styles.textInput}
                onChangeText={text => setValueEight(text)}
              />
            </View>
            <TouchableOpacity
              style={styles.addBtn}
              onPress={() => saveTeamMembers()}>
              <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task Id</Text>
          <TextInput
            value={valueOne}
            style={styles.textInput}
            onChangeText={text => setValueOne(text)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task name</Text>
          <TextInput
            value={valueFour}
            style={styles.textInput}
            onChangeText={text => setValueTwo(text)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>
            Task description
          </Text>
          <TextInput
            value={valueThree}
            style={styles.textInput}
            onChangeText={text => setValueThree(text)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>
            Task start date
          </Text>
          <TextInput
            value={valueSix}
            style={styles.textInput}
            onChangeText={text => setValueSix(text)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginLeft: 10,
          }}>
          <Text style={{alignSelf: 'center', width: '35%'}}>Task end date</Text>
          <TextInput
            value={valueFive}
            style={styles.textInput}
            onChangeText={text => setValueFive(text)}
          />
        </View>
        <Picker
          selectedValue={selectedValue}
          style={{height: 50, width: 150}}
          onValueChange={(itemValue, itemIndex) => openModal(itemValue)}>
          {users.map(x => {
            return <Picker.Item label={x.firstname} value={x.email} />;
          })}
        </Picker>
        <TouchableOpacity
          style={styles.textInputAddBtn}
          onPress={() => saveTaskData()}>
          <Text style={[styles.addText, {color: 'white'}]}>Save</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  addBtn: {
    margin: 10,
    width: '95%',
    height: 45,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  addText: {
    alignSelf: 'center',
  },
  textInputView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textInput: {
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: '60%',
    height: 40,
    borderColor: '#000',
  },
  textInputAddBtn: {
    margin: 10,
    padding: 10,
    height: 50,
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  touchableOpacityStyle: {
    height: 80,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
  },
  textStyle: {
    margin: 10,
  },
});

export default NewTask;
